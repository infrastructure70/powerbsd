# Vars, cannot be changed via environment

# Vars, can be changed via the environment
APP?=freebsd-custom
ARCH?=amd64
BRANCH?=$(shell git rev-parse --abbrev-ref HEAD)
BUILD_DIR?=bin
HACK_DIR?=hack
LOG_DIR?=doc/log/build_history
RELEASE?=false

# Vars, automatically populated
COMMIT=$(shell git rev-parse --verify 'HEAD^{commit}')
CURR_GO_VERSION=$(shell go version | grep go | cut -d " " -f 3)
SEMVER=$(shell git describe --always --abbrev=0 --dirty)
VERSION=$(shell git describe --always --abbrev=40 --dirty)

# Complex Vars, built from the variables above and environment variables
FILE_ARCH=$(ARCH)
BUILD_PATH=$(BUILD_DIR)/$(SEMVER)/$(FILE_ARCH)
LOG_PATH=$(LOG_DIR)/$(SEMVER)/$(FILE_ARCH)


.PHONY: default branch build clean code-prep commit dir-prep doc install pr pr-close release shellcheck version yaml-lint
.EXPORT_ALL_VARIABLES:

default: clean doc code-prep build

# Make a new branch
branch:
	@hack/branch.sh

# Build binary
build: dir-prep 
	@hack/build.sh

# Delete old build directory, clear out old log
clean:
	@hack/clean.sh

# Prepare the code for building
code-prep: shellcheck yaml-lint

# Create a new commit for all of the changes in the current directory
commit: clean dir-prep code-prep doc
	@hack/commit.sh

# Prepare build and log directories
dir-prep:
	@hack/dir-prep.sh

# Build templated documentation
doc:
	@hack/doc.sh

# Make a PR for the current branch or branch specified with `BRANCH=branch make pr`
pr:
	@hack/pr.sh

# Merge current branch or branch specified with `BRANCH=branch make pr-close` to master
pr-close:
	@hack/pr-close.sh

# Build release
release: clean dir-prep code-prep doc
	@hack/release.sh

render:
	@hack/render.sh

# Run shellcheck
shellcheck:
	@hack/shellcheck.sh

# Output version
version:
	@echo $(VERSION)

# Run YAML linter
yaml-lint:
	@hack/yaml-lint.sh
